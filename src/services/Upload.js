import axios from 'axios';
//  id
//  secret
const ImgurClientId = 'e700afed2e225ec';
// const ImgurSecret = '200d5ebed72a07e61e70eef5e79c0a6977ebd297';
export default {
	upload: async (formData, onUploadProgress = () => {}) => {
		return axios
			.post(`https://api.imgur.com/3/upload`, formData, {
				headers: {
					Authorization: `Client-ID ${ImgurClientId}`
				},
				onUploadProgress
			})
			
			.then((res) => res.data);
	}
};
