import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './Routes';

Vue.use(VueRouter);
// init add router middleware
export default new VueRouter({ routes });
