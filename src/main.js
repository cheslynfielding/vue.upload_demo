import Vue from 'vue';
import 'bootstrap/dist/css/bootstrap.min.css';

import App from './App';
import Router from './router/Router';

Vue.config.productionTip = false;

new Vue({
	render: (h) => h(App),
	router: Router,
	components: { App }
}).$mount('#app');
